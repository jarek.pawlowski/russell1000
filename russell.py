import utils
# other imports are in utils.py file

def main():
    # get Russell1000 index timeseries for april 2019
    rui_april_series = utils.grab_timeseries(utils.rui_ticker, utils.rui_start, utils.rui_end)
    rui_april_series = utils.return_rate(rui_april_series)
    #utils.plot_timeseries(rui_april_series, utils.rui_ticker, utils.rui_days)

    # get Russell1000 portfolio as 31mar2019 from iShares
    rui_portfolio_tickers, rui_portfolio_prices, rui_portfolio_shares = utils.grab_portfolio(
        utils.russell1000as31mar2019_portfolio_url, utils.russell1000_portfolio_cvs)
    # take 25 largest components
    rui_portfolio25_tickers = rui_portfolio_tickers[:25]
    # collect series for given components
    rui_portfolio25_series = utils.grab_timeseries(rui_portfolio25_tickers, utils.rui_start, utils.rui_end)
    # normalize timeseries by setting 1st value to one
    rui_portfolio25_series = utils.return_rate(rui_portfolio25_series)
    # plot rui + its portfolio
    rui_and_portfolio_series = [rui_april_series] + rui_portfolio25_series
    rui_and_portfolio_tickers = [utils.rui_ticker] + rui_portfolio25_tickers
    utils.plot_timeseries(rui_and_portfolio_series, rui_and_portfolio_tickers, utils.rui_days)

    # naive approach: average of series in portfolio
    weights = [1.]*25
    model0 = utils.model_index(rui_portfolio25_series, weights)
    print("tracking error for naive model: " + str(utils.tracking_error(model0, rui_april_series)))

    # benchmark model
    weights = rui_portfolio_prices[:25]*rui_portfolio_shares[:25]
    benchmark = utils.model_index(rui_portfolio25_series, weights)
    print("tracking error for benchmark model: " + str(utils.tracking_error(benchmark, rui_april_series)))

    # ad_hoc model
    weights1 = weights + 1.9/25
    model1 = utils.model_index(rui_portfolio25_series, weights1)
    print("tracking error for ad_hoc model: " + str(utils.tracking_error(model1, rui_april_series)))

    # improved model, by taking into account past correlations with elements 25..50 in portfolio...
    N = 50
    # take N largest components
    rui_portfolioN_tickers = rui_portfolio_tickers[:N]
    # collect series for given components - two months back works best
    rui_portfolioN_series = utils.grab_timeseries(rui_portfolioN_tickers, "2019-02-01", "2019-03-29")
    # normalize timeseries by setting 1st value to one
    rui_portfolioN_series = utils.return_rate(rui_portfolioN_series)

    import numpy as np
    # calculate the correlation matrix
    c_matrix = np.corrcoef(np.array(rui_portfolioN_series))[25:N,:25]
    # normalize
    for i in range(N-25):
        norm = np.linalg.norm(c_matrix[i,:])
        c_matrix[i,:] /= norm

    # ...improved model
    weights = rui_portfolio_prices[:N]*rui_portfolio_shares[:N]
    weights /= np.sum(weights)
    weights0_25 = weights[:25]
    weights25_N = weights[25:N]
    weights_correction = np.zeros(25)
    for i in range(25):
        for j in range(N-25):
            weights_correction[i] += weights25_N[j]*c_matrix[j,i]

    model2 = utils.model_index(rui_portfolio25_series, weights0_25 + weights_correction)
    print("tracking error for improved model: " + str(utils.tracking_error(model2, rui_april_series)))

    # models comparison
    utils.plot_timeseries([rui_april_series, model0, model1, model2, benchmark],
                          [utils.rui_ticker,'naive','ad_hoc','improved','benchmark'], utils.rui_days)

if __name__ == '__main__':
    main()







