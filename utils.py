# grab data
import yfinance as yf
from yahoofinancials import YahooFinancials as yfs
from urllib.request import urlretrieve
import threading
# process data
import numpy as np
import pandas as pd
# plot data
import matplotlib.pyplot as plt

# constants
# - we grab portfolio data from iShares
russell1000as31mar2019_portfolio_url = 'https://www.ishares.com/us/products/239707/ishares-russell-1000-etf/\
1467271812596.ajax?fileType=csv&fileName=IWB_holdings&dataType=fund&asOfDate=20190329'
russell1000as28feb2019_portfolio_url = 'https://www.ishares.com/us/products/239707/ishares-russell-1000-etf/\
1467271812596.ajax?fileType=csv&fileName=IWB_holdings&dataType=fund&asOfDate=20190228'
russell1000as31jan2019_portfolio_url = 'https://www.ishares.com/us/products/239707/ishares-russell-1000-etf/\
1467271812596.ajax?fileType=csv&fileName=IWB_holdings&dataType=fund&asOfDate=20190131'
russell1000_portfolio_cvs = 'Russell1000_portfolio.csv'
# - and required timeseries from yahoo finance
rui_ticker = "^RUI"
rui_start = "2019-04-01"
rui_end = "2019-04-30"
rui_series = yf.download(rui_ticker, rui_start, rui_end)
rui_price = rui_series['Close'].to_numpy()
rui_length = rui_price.size
# subsequent days in april 2019
rui_days = pd.to_datetime(rui_series.index.values).day.to_list()
rui_days = list(map(str,rui_days))
# threading for massive series downloading
downloaded_series = None
result_available = threading.Event()

def ydownload(tick, time_series_start, time_series_end):
    global downloaded_series
    downloaded_series = yf.download(tick, time_series_start, time_series_end)['Close'].to_numpy()
    result_available.set()

def grab_timeseries(tickers, time_series_start, time_series_end):
    """
    :param tickers: singe ticker or list of tickers of series to downolad from yahoo-finance
    :param time_series_start: series starting day
    :param time_series_end: ending day
    :return: single or list of series within specified period
    """
    if isinstance(tickers, (list, np.ndarray)):
        timeseries = []
        for tick in tickers:
            thread = threading.Thread(target = ydownload(tick, time_series_start, time_series_end))
            thread.start()
            result_available.wait()
            timeseries.append(downloaded_series)
    else:
        timeseries = yf.download(tickers, time_series_start, time_series_end)['Close'].to_numpy()
    return timeseries

def return_rate(timeseries):
    """
    :param timeseries: single or list of series to calculate rate of return
    :return: rate of return for series
    """
    if isinstance(timeseries, list):
        for i, singleseries in enumerate(timeseries):
            timeseries[i] = (singleseries/singleseries[0] - 1.)*100
    else:
        timeseries = (timeseries/timeseries[0] - 1.)*100
    return timeseries

def tracking_error(series, benchmark):
    """
    standard devition of differences between modelel and benchmark series
    """
    div = series - benchmark
    return np.std(div, ddof=1)

def grab_portfolio(portfolio_url, portfolio_cvs):
    urlretrieve(portfolio_url, portfolio_cvs)
    with open(portfolio_cvs, 'r') as portfolio_buffer:
        portfolio = pd.read_csv(portfolio_buffer, header=9)
        portfolio_tickers = portfolio['Ticker'].tolist()
        # eliminate some mismatches in ticknames
        ticks_dict = {"BRKB": "BRK-B","DWDP": "DD"}
        portfolio_tickers = [ticks_dict.get(item, item) for item in portfolio_tickers]
        portfolio_prices = portfolio['Price'].to_list()
        portfolio_prices = [float(str(price).replace(',','')) for price in portfolio_prices]
        portfolio_prices = np.array(portfolio_prices)
        portfolio_shares = portfolio['Shares'].to_list()
        portfolio_shares = [float(str(share).replace(',', '')) for share in portfolio_shares]
        portfolio_shares = np.array(portfolio_shares).astype(np.int)
    return portfolio_tickers, portfolio_prices, portfolio_shares

def model_index(portfolio_series, weights):
    """
    :param portfolio_series: portfolio of series that enter to the index model
    :param weights: assumed weights for each of series
    :return: index model
    """
    index = np.zeros(portfolio_series[0].size)
    weights /= np.sum(weights)
    for i, singleseries in enumerate(portfolio_series):
        index += weights[i] * singleseries
    return index

def plot_timeseries(timeseries, tickers, timeseries_days):
    """
    :param timeseries: single or list of series to plot
    :param tickers: single or list of tickers of given series
    :param time_series_days: list of labels for time-axis
    :return: plot
    """
    fig, ax = plt.subplots()
    ax.set_xlabel("subsequent days in april 2019")
    ax.set_xticklabels(timeseries_days)
    ax.set_ylabel("rate of return (%)")
    if isinstance(tickers, (list, np.ndarray)):
        timeseries_length = timeseries[0].size
        ax.set_xticks(np.arange(timeseries_length))
        cmap = plt.get_cmap('viridis') if len(tickers) < 10 else plt.get_cmap('hsv')
        colors = [cmap(i) for i in np.linspace(0, 1, len(timeseries))]
        for i, series in enumerate(timeseries):
            style = 'solid' if i == 0 else 'dotted'
            ax.plot(series, color=colors[i], label=tickers[i], linestyle=style)
        plt.legend(bbox_to_anchor=(1.0,1.0), fontsize=6)
    else:
        timeseries_length = timeseries.size
        ax.set_xticks(np.arange(timeseries_length))
        ax.plot(timeseries, color='orange')
    plt.savefig("series.png", format='png', dpi=300, bbox_inches='tight')
    plt.show()

#yahoo_financials = yfs('MSFT')
#print(yahoo_financials.get_num_shares_outstanding(price_type='current'))